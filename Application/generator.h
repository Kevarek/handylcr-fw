/*
 * generator.h
 *
 *  Created on: Oct 28, 2019
 *      Author: Standa
 */

#ifndef GENERATOR_H_
#define GENERATOR_H_

#include "error.h"


extern void gen_Init(void);
extern err_Td gen_Prepare(float freq_Hz);
extern err_Td gen_Start(void);
extern void gen_Stop(void);
extern uint8_t gen_GetState(void);
extern float gen_GetActualFreq(void);


#endif /* GENERATOR_H_ */
