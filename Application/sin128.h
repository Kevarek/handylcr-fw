/*
 * sin128.h
 *
 *  Created on: Oct 27, 2019
 *      Author: Standa
 */

#ifndef SIN128_H_
#define SIN128_H_

#define SIN128_CNT	128

extern float Sin128[];


extern void sin128_Init(void);
extern float Sin128_Interpolate(float x);
extern float Sin128_InterpolateCos(float x);


#endif /* SIN128_H_ */
