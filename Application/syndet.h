/*
 * syndet.h
 *
 *  Created on: Oct 24, 2019
 *      Author: Standa
 */

#ifndef SYNDET_H_
#define SYNDET_H_

#include "error.h"
#include "utility.h"


typedef struct{
	float Freq_Hz;
	uint32_t FMI;
	ut_MeanStdevTd* V;
	ut_MeanStdevTd* VFi;
	ut_MeanStdevTd* I;
	ut_MeanStdevTd* IFi;
	ut_MeanStdevTd* Z;
	ut_MeanStdevTd* Zfi;
	ut_MeanStdevTd* R;
	ut_MeanStdevTd* X;
	ut_MeanStdevTd* C;
	ut_MeanStdevTd* L;
	ut_MeanStdevTd* D;
	ut_MeanStdevTd* Q;

	ut_MeanStdevTd* ClipVlow;
	ut_MeanStdevTd* ClipVhigh;
	ut_MeanStdevTd* ClipIlow;
	ut_MeanStdevTd* ClipIhigh;
} sd_Results;


extern void sd_init(void);
extern void sd_StopMeasurement(void);
extern err_Td sd_HandleMeasurementSync(float fm_Hz, uint16_t desiredCycles, uint8_t dropCycles);
extern uint8_t sd_GetMeasurementStatus(void);
extern sd_Results* sd_GetMeasurementResults();
extern float sd_GetMeasFreq(void);
extern uint8_t sd_GetMeasurementStatus(void);

extern void meas_SynchronizationHandler(void);
extern void meas_EndOfADCReadingHandler(void);

#endif /* SYNDET_H_ */
