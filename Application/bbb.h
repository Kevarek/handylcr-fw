/*
 * bbb.h
 *
 *  Created on: Nov 25, 2019
 *      Author: Standa
 */

#ifndef BBB_H_
#define BBB_H_

#include <stdint.h>


typedef enum{
	bbb_ToneTd_None = 0,
	bbb_ToneTd_Low = 1,
	bbb_ToneTd_Med = 2,
	bbb_ToneTd_High = 3,
}bbb_ToneTd;


typedef enum{
	bbb_EvtTd_Low = -2,
	bbb_EvtTd_High = -1,
	bbb_EvtTd_None = 0,
	bbb_EvtTd_Pressed,
	bbb_EvtTd_Released,
	bbb_EvtTd_SingleReleased,
	bbb_EvtTd_DoubleReleased,
	bbb_EvtTd_LongReleased,
}bbb_EvtTd;


typedef struct{
	float LowerThreshold;
	float UpperThreshold;
	uint32_t ReleasedTimestamp;
	uint32_t StateChangeInterval;
	uint32_t SingleReleasedEnsuredTimestamp;
	uint32_t PrevBtnStateTimestamp;
	bbb_EvtTd PrevBtnEvt;
	uint8_t PrevBtnState;
}bbb_BtnTd;


extern bbb_BtnTd BtnUp, BtnDn;


extern void bbb_Init(void);

extern uint8_t bbb_UpdateBtns(void);
extern float bbb_GetVbat(void);
extern float bbb_GetTemp(void);


extern void bbb_Beep(bbb_ToneTd tone, uint16_t time);
extern void bbb_Melody2(uint8_t tone1, uint8_t tone2, uint16_t time);
extern void bbb_Melody3(uint8_t tone1, uint8_t tone2, uint8_t tone3, uint16_t time);

#endif /* BBB_H_ */
