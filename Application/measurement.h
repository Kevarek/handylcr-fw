/*
 * measurement.h
 *
 *  Created on: Oct 28, 2019
 *      Author: Standa
 */

#ifndef MEASUREMENT_H_
#define MEASUREMENT_H_

#include "sin128.h"
#include "error.h"

#define MEAS_FMIN_HZ				1
#define MEAS_FMAX_HZ				1000
#define MEAS_READSPERMEASPERIOD		SIN128_CNT					//Number of ADC readings per measurement period
#define MEAS_ADCRANGE						4096				//12bit adc - 2^12; todo should not be placed here!!! remove


typedef struct{
	float VBBB_V;
	float Temp_Deg;
	float Vcc_V;
	float V10R_V;
	float V100R_V;
	float IM_V;
}meas_InputTd;


extern void meas_Init(void);
extern err_Td meas_Prepare(float freq_Hz, float startDelay_perc);
extern err_Td meas_Start(void);
extern err_Td meas_Continue(void);
extern void meas_Stop(void);
extern uint8_t meas_GetState(void);
extern uint16_t* meas_GetDataBuffer(void);
extern void meas_SetCycleFinishedHook(void (*cycleFinishedHook)(void));

extern void meas_SynchronizationHandler(void);
extern void meas_EndOfADCReadingHandler(void);

extern err_Td meas_ReadAnalogInputsSync_V(meas_InputTd *results);
#endif /* MEASUREMENT_H_ */
