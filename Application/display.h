/*
 * display.h
 *
 *  Created on: Nov 27, 2019
 *      Author: Standa
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "syndet.h"

extern void disp_DrawInitScreen(float temp, float vbat);
extern void disp_DrawProggressScreen(float temp, float vbat);
extern void disp_DrawResultsScreen(sd_Results *res, uint8_t isStdevRequested);

extern void disp_DrawFrequencyPopup(float freq);


#endif /* DISPLAY_H_ */
