/*
 * sin128.c
 *
 *  Created on: Oct 27, 2019
 *      Author: Standa
 *	Provides sin and cos functions with limited input range +-2PI
 *	Low memory footprint is the target, performance not optimized (tested)
 */

#include <sin128.h>
#include <stdint.h>
#include "qfplib.h"


#define SIN128_ANGLESTEP_RAD	0.049087385		//	2*PI/SIN128_CNT

float Sin128[SIN128_CNT] = {
	//0,0.049067674,0.09801714,0.146730474,0.195090322,0.24298018,0.290284677,0.336889853,0.382683432,0.427555093,0.471396737,0.514102744,0.555570233,0.595699304,0.634393284,0.671558955,0.707106781,0.740951125,0.773010453,0.803207531,0.831469612,0.85772861,0.881921264,0.903989293,0.923879533,0.941544065,0.956940336,0.970031253,0.98078528,0.98917651,0.995184727,0.998795456,1,0.998795456,0.995184727,0.98917651,0.98078528,0.970031253,0.956940336,0.941544065,0.923879533,0.903989293,0.881921264,0.85772861,0.831469612,0.803207531,0.773010453,0.740951125,0.707106781,0.671558955,0.634393284,0.595699304,0.555570233,0.514102744,0.471396737,0.427555093,0.382683432,0.336889853,0.290284677,0.24298018,0.195090322,0.146730474,0.09801714,0.049067674,1.22515E-16,-0.049067674,-0.09801714,-0.146730474,-0.195090322,-0.24298018,-0.290284677,-0.336889853,-0.382683432,-0.427555093,-0.471396737,-0.514102744,-0.555570233,-0.595699304,-0.634393284,-0.671558955,-0.707106781,-0.740951125,-0.773010453,-0.803207531,-0.831469612,-0.85772861,-0.881921264,-0.903989293,-0.923879533,-0.941544065,-0.956940336,-0.970031253,-0.98078528,-0.98917651,-0.995184727,-0.998795456,-1,-0.998795456,-0.995184727,-0.98917651,-0.98078528,-0.970031253,-0.956940336,-0.941544065,-0.923879533,-0.903989293,-0.881921264,-0.85772861,-0.831469612,-0.803207531,-0.773010453,-0.740951125,-0.707106781,-0.671558955,-0.634393284,-0.595699304,-0.555570233,-0.514102744,-0.471396737,-0.427555093,-0.382683432,-0.336889853,-0.290284677,-0.24298018,-0.195090322,-0.146730474,-0.09801714,-0.049067674
};


void sin128_Init(void){
	for(uint16_t i=0; i<SIN128_CNT; i++){
		Sin128[i] = qfp_fsin( qfp_fmul(i, SIN128_ANGLESTEP_RAD) );
	}
}

/*
 * Returns sin x, where x [rad] is in range +-2PI
 * For given x range, maximum error (InterpolatedSin - TrueSin) is in range <-0.0003;0.0003>
 */
/*
float Sin128_Interpolate(float x){
	float AngleInc = SIN128_ANGLESTEP_RAD;
	int8_t Sign = 1;

	if(x<0){
		Sign = -1;
		x *= -1;
	}

	for(uint8_t i=0; i< SIN128_CNT-1; i++){
		if(x >= i*AngleInc && x < (i+1)*AngleInc){
			float DY = (Sin128[i+1] - Sin128[i]) / AngleInc;
			return (Sin128[i] + DY * (x-i*AngleInc))*Sign;
		}
		else if(i == SIN128_CNT-2){ //Interpolation between last point in the table and zero point
			float DY = (Sin128[0] - Sin128[i+1]) / AngleInc;
			return (Sin128[i] + DY * (x-i*AngleInc))*Sign;
		}
	}
	return NAN;
}
*/

/*
 * Returns cos x, where x [rad] is in range +-2PI
 * For given x range, maximum error (InterpolatedCos - TrueCos) is in range <-0.0003;0.0003>
 */
/*
float Sin128_InterpolateCos(float x){
	if(x <= M_TWOPI-M_PI_2){
		return Sin128_Interpolate(x+M_PI_2);
	}
	else{
		return Sin128_Interpolate(M_TWOPI-x + M_PI_2);
	}
}
*/


/*
 * Below is benchmark to tests interpolated sine performance
 */

/*
float MinSinErr = 1000, MaxSinErr = -1000;
for(float x=-M_TWOPI; x<=M_TWOPI; x+= 0.01){
	float SinVal = sinf(x);
	float SinValInterpol = Sin128_Interpolate(x);
	float Sinerr = SinValInterpol - SinVal;

	if(Sinerr>MaxSinErr) MaxSinErr =  Sinerr;
	if(Sinerr<MinSinErr) MinSinErr = Sinerr;
}
*/
