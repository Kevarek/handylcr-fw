/*
 * calibration.h
 *
 *  Created on: Nov 20, 2019
 *      Author: Standa
 */

/* CALIBRATION MEASUREMENT REPORT

R23			0R
R26         329K95
R29         32K94
R31         33K06

R32         330K25
R33         329K35
R35			0R
R36         32K85

R30         150R4

PROBERESISTANCE=0.2
	PROBERESISTANCE = 0.2
R100GAIN=(329.95-PROBERESISTANCE)/(32.94-PROBERESISTANCE+33.06-PROBERESISTANCE)
	R100GAIN = 5.026676829
R10GAIN=(330.25-PROBERESISTANCE+329.35-PROBERESISTANCE)/(32.85-PROBERESISTANCE)
	R10GAIN = 20.189892802
IGAIN=(150.4-PROBERESISTANCE)	//R30
	IGAIN = 150.2

	Resistor R30 changed (two in parallel)

	1/329.95+1/1001.7
		Ans = 0.004029065
	1/Ans
		Ans = 248.196534375
*/

#ifndef CALIBRATION_H_
#define CALIBRATION_H_

#include "utility.h"


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   SELECT CORRECT CALIBRATION PHASE, START FROM 0, PROCEED TO MAX   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//#define CAL_CALIBRATION_PHASE	0	//No calibration
//#define CAL_CALIBRATION_PHASE	1	//Voltage and current amplifier gains calculated (from measured resistors during the assembly)
//#define CAL_CALIBRATION_PHASE	2	//Everything above and also VREF calibration is used
//#define CAL_CALIBRATION_PHASE	3	//Everything above and also short calibration used
#define CAL_CALIBRATION_PHASE	4	//Everything above and also slope calibration used (measured data from 10R and 100R reference resistors)


//In chronological order



//Calculated after the build (resistor values measured with DMM during the assembly)
#if CAL_CALIBRATION_PHASE >= 1
	#define CAL_R10RGAIN						20.189892802f		//Calculated gain of 10R range sensing amplifier
	#define CAL_R100RGAIN						5.026676829f		//Calculated gain of 100R range sensing amplifier
	#define CAL_IMGAIN							248.196534375f		//Calculated gain of current measurement sensing amplifier
#else
	#define CAL_R10RGAIN						20.0f		//Calculated gain of 10R range sensing amplifier
	#define CAL_R100RGAIN						5.0f		//Calculated gain of 100R range sensing amplifier
	#define CAL_IMGAIN							250.0f		//Calculated gain of current measurement sensing amplifier
#endif



//Measured after first flashing
#if CAL_CALIBRATION_PHASE >= 2
	#define CAL_VREF_25DEG_V					3.304f				//Voltage of the 3V3 regulator during room temperature
#else
	#define CAL_VREF_25DEG_V					3.300f				//Voltage of the 3V3 regulator during room temperature
#endif



//Measured during first calibration (without any calibration loaded)
#if CAL_CALIBRATION_PHASE >= 3
	#define CAL_Z10SHORT_RS						0.025f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
	#define CAL_Z10SHORT_XS						0.000f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
	#define CAL_Z100SHORT_RS					0.019f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
	#define CAL_Z100SHORT_XS					0.000f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)

#else
	#define CAL_Z10SHORT_RS						0.000f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
	#define CAL_Z10SHORT_XS						0.000f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
	#define CAL_Z100SHORT_RS					0.000f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
	#define CAL_Z100SHORT_XS					0.000f				//Short circuit calibration (compensates for series resistance and inductance which is zero in this case for such a low frequencies)
#endif



//Measured after short calibration (short calibration in use)
#if CAL_CALIBRATION_PHASE >= 4
#define CAL_Z10SLOPECOMP						1.0062f				//After
#define CAL_Z100SLOPECOMP						1.0059f				//After
#else
#define CAL_Z10SLOPECOMP						1.000f
#define CAL_Z100SLOPECOMP						1.000f
#endif

#endif /* CALIBRATION_H_ */
